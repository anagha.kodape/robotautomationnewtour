*** Settings ***
Library  SeleniumLibrary
Resource    ../config/newtour_locators.robot
Resource    ../utility/newtourKeywords.robot 
  
*** Test Cases ***
LoginTest
    [Documentation]    This Test case is for login
    Open my Browser    
    Enter Username    
    Enter Password    
    Click Signin
    Sleep    3 seconds    
    Verify Successful Login
    Close my Browser
    
RegistrationTest
    [Documentation]    This Testcase is for Registration
    Open my Browser
    Click Register Link
    Sleep    3 seconds  
    Enter FirstName
    Enter LastName
    Enter Phone
    Enter Email
    Enter Address
    Enter City
    Enter State
    Enter Postal
    Enter Country
    Enter Register User Name
    Enter Register Password
    Enter Register Confirm Password
    Click Submit
    Verify Succesful Registration
    Close my Browser