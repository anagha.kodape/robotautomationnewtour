*** Settings ***
library     SeleniumLibrary
Resource    ../config/newtour_locators.robot

*** Keywords ***

Open my Browser
    #[Arguments]    ${SiteURL}    ${Browser}
    Open Browser    ${SiteURL}    ${Browser}   
    Maximize Browser Window
    
#Login Keywords    
Enter Username
    #[Arguments]    ${username}
    Input Text    ${username}    ${txt_user}
Enter Password
    #[Arguments]    ${password}
    Input Text    ${password}    ${txt_password}          
Click Signin
    Click Button    ${loginbtn}    
Verify Successful Login
    Title Should Be    Login: Mercury Tours 
Close my Browser   
    Close All Browsers

# Registration Keywords

Click Register Link
    Click Link    ${link_register}
    
Enter FirstName
    Input Text    ${txt_firstName}    ${firstName}    
    
Enter LastName
    Input Text    ${txt_lastName}    ${lastName} 
    
Enter Phone
    Input Text    ${txt_phone}    ${phone}    

Enter Email
    Input Text    ${txt_email}    ${email}   
    
Enter Address
    Input Text    ${txt_address}    ${address}

Enter City
    Input Text    ${txt_city}    ${city}
    
Enter State
    Input Text    ${txt_state}    ${state}    
    
Enter Postal
    Input Text    ${txt_postal}    ${postal}    
    
Enter Country
    Select From List By Label    ${drop_country}    ${country}
    
Enter Register User Name
    Input Text    ${txt_rusername}    ${rusername} 
     
Enter Register Password
    Input Text    ${txt_rpassword}    ${rpassword}   
    
Enter Register Confirm Password
    Input Text    ${txt_confirmpassword}    ${confirmpassword}   
    
Click Submit
    Click Element    ${btn_submit}    
    
Verify Succesful Registration
    Page Should Contain    Thank you for registering
    



