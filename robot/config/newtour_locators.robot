*** Variables ***

# URL variables
${Browser}   firefox
${SiteURL}    https://demo.guru99.com/test/newtours/

# Login variables
${txt_user}    mercury  
${txt_password}    mercury

# Registration variables
${firstName}    Anagha    
${lastName}    Kodape    
${phone}    123456789
${email}    ak@gmail.com    
${address}    Baner
${city}    Pune
${state}    Maharashtra    
${postal}    411045
${country}    INDIA
${rusername}    akodape    
${rpassword}    akodape
${confirmpassword}    akodape

# Locators of Login page
${username}    //input[@name='userName']  
${password}    //input[@name='password'] 
${loginbtn}    //input[@name='submit']

# Locators of Registration page

${link_register}    link:REGISTER
${txt_firstName}    name:firstName
${txt_lastName}    name:lastName  
${txt_phone}    name:phone
${txt_email}    xpath://input[@id='userName']
${txt_address}    name:address1
${txt_city}    name:city
${txt_state}    name:state
${txt_postal}    name:postalCode
${drop_country}    name:country
${txt_rusername}    name:email
${txt_rpassword}    name:password
${txt_confirmpassword}    name:confirmPassword
${btn_submit}    name:submit 

 

